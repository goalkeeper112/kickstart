var datos = new Array();

function comando(req, res){
	res.render('../correo.html'.{
		datos: datos
	});
}

exports.get_comando = function(req, res){
	comando(req, res);
}

exports.post_comando = function(req, res) {
	var servicio = req.body.servicio;
	var correo   = req.body.correo;
	datos.push({
		servicio: servicio,
		correo: correo
	})
	comando(req, res);
}